# frozen_string_literal: true

require "date"

# Bank account class
class BankAccount
  attr_reader :balance

  ERRORS = {
    invalid_amount: {
      status: :fail,
      error: "invalid amount"
    },
    insufficient_funds: {
      status: :fail,
      error: "insufficient funds"
    }
  }.freeze

  def initialize(amount = 0)
    raise ArgumentError, "invalid balance" unless amount.is_a?(Integer)
    @balance = amount
    @movements = []
  end

  def deposit(amount, date)
    return ERRORS[:invalid_amount] if !amount.is_a?(Integer) || amount.zero?
    @balance += amount
    @movements << {
      amount: amount,
      date: Date.parse(date),
      operation: :deposit
    }
    operation_succeeded
  end

  def withdrawal(amount, date)
    return ERRORS[:invalid_amount] if !amount.is_a?(Integer) || amount.zero?
    return ERRORS[:insufficient_funds] if @balance < amount
    @balance -= amount
    @movements << {
      amount: amount,
      date: Date.parse(date),
      operation: :withdrawal
    }
    operation_succeeded
  end

  def statement
    {
      movements: @movements.sort_by { |movement| movement[:date] },
      balance: balance
    }
  end

  private

  def operation_succeeded
    {
      status: :ok,
      balance: @balance
    }
  end
end
