# frozen_string_literal: true

require "minitest/autorun"
require_relative "bank_account"

# Tests for the BankAccount class
class BankAccountTest < MiniTest::Test
  def test_initializer_wihtout_balance
    bank_account = BankAccount.new
    assert(bank_account.balance.zero?)
  end

  def test_initializer_wiht_valid_balance
    bank_account = BankAccount.new(12)
    assert(!bank_account.balance.zero?)
    assert_equal(bank_account.balance, 12)
  end

  def test_initializer_wiht_malformed_balance
    assert_raises ArgumentError do
      BankAccount.new("squirrel")
    end
  end

  def test_correct_deposit
    bank_account = BankAccount.new
    assert_equal(bank_account.deposit(12, "01-01-2018"), status: :ok, balance: 12)
    assert_equal(bank_account.balance, 12)
  end

  def test_correct_deposit_with_balance
    bank_account = BankAccount.new(10)
    assert_equal(bank_account.deposit(12, "01-01-2018"), status: :ok, balance: 22)
    assert_equal(bank_account.balance, 22)
  end

  def test_incorrect_deposit_0
    bank_account = BankAccount.new
    assert_equal(bank_account.deposit(0, "01-01-2018"), status: :fail, error: "invalid amount")
    assert(bank_account.balance.zero?)
  end

  def test_incorrect_deposit_string
    bank_account = BankAccount.new
    assert_equal(bank_account.deposit("boar", "01-01-2018"), status: :fail, error: "invalid amount")
    assert(bank_account.balance.zero?)
  end

  def test_correct_withdrawal
    bank_account = BankAccount.new(40)
    assert_equal(bank_account.withdrawal(10, "01-01-2018"), status: :ok, balance: 30)
    assert_equal(bank_account.balance, 30)
  end

  def test_incorrect_withdrawal_0
    bank_account = BankAccount.new(40)
    assert_equal(bank_account.withdrawal(0, "01-01-2018"), status: :fail, error: "invalid amount")
    assert_equal(bank_account.balance, 40)
  end

  def test_incorrect_withdrawal_string
    bank_account = BankAccount.new(40)
    assert_equal(bank_account.withdrawal("dog", "01-01-2018"), status: :fail, error: "invalid amount")
    assert_equal(bank_account.balance, 40)
  end

  def test_incorrect_withdrawal_insufficient_funds
    bank_account = BankAccount.new(10)
    assert_equal(bank_account.withdrawal(20, "01-01-2018"), status: :fail, error: "insufficient funds")
    assert_equal(bank_account.balance, 10)
  end

  def test_statement
    bank_account = BankAccount.new
    bank_account.deposit(12, "01-01-2018")
    bank_account.withdrawal(2, "02-05-2018")
    bank_account.deposit(22, "01-02-2018")
    bank_account.withdrawal(30, "01-11-2017")
    assert_equal(
      bank_account.statement,
      movements: [
        {
          amount: 30,
          date: Date.parse("01-11-2017"),
          operation: :withdrawal
        },
        {
          amount: 12,
          date: Date.parse("01-01-2018"),
          operation: :deposit
        },
        {
          amount: 22,
          date: Date.parse("01-02-2018"),
          operation: :deposit
        },
        {
          amount: 2,
          date: Date.parse("02-05-2018"),
          operation: :withdrawal
        }
      ],
      balance: 2
    )
  end
end
